﻿using System;

namespace dotnetcore
{
    using amdGpu;
    using nvidiaGpu;
    using memory;
    class Program
    {
        static void Main(string[] args)
        {
            amdGpu agpu = new amdGpu();
            nvidiaGpu ngpu = new nvidiaGpu();

            Console.WriteLine(agpu.rendering());
            Console.WriteLine(ngpu.rendering());
            Console.WriteLine(ngpu.raytracing(5));

            memory[] mems = {new memory(50) , new memory(55), new memory(100), new memory(32),new memory(64)};

            Array.Sort(mems, (x,y) => x.size.CompareTo(y.size));
            
            for (int i = 0; i < 5; i++) 
            {
                Console.WriteLine(mems[i].size);
            }
        }
    }
}
