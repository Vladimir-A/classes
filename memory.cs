using System;

namespace memory
{
    public class  status : Attribute
    {
        public status () {}

        public status (string name) 
        {
            Name = name;
        }

        public string Name {get; set;}

    }

    [status("memory")]
     public class memory : Attribute
    {
        public memory () {}

        public memory (float s) {
            size = s;
        }

        public float timing {get; set;}
        public float size {get; set;}
    }
}